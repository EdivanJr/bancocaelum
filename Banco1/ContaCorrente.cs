﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banco1
{
    public class ContaCorrente : Conta
    {
        
        public override void Deposita(double valorOperacao)
        {
            base.Deposita(valorOperacao -0.10);
        }
        public override void Saca(double valor)
        {
            base.Saca(valor +0.05);
        }
    }
}
