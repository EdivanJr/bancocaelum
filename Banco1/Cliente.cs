﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banco1
{
    public class Cliente
    {
        public string Nome { get; set; }
        public int Idade { get; set; }
        public string cpf { get; set; }

        public Cliente(string nome)
        {
            this.Nome = nome;
        }       
    }
}
