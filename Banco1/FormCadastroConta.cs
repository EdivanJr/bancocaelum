﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Banco1
{
    public partial class FormCadastroConta : Form
    {
        private readonly Form1 _formPrincipal;
     
        public FormCadastroConta(Form1 formPrincipal)
        {
            this._formPrincipal = formPrincipal;
            InitializeComponent();
            comboTipoConta.Items.Add("Corrente");
            comboTipoConta.Items.Add("Poupança");
        }
        public FormCadastroConta()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void botaoCadastro_Click(object sender, EventArgs e)
        {
            if (comboTipoConta.SelectedIndex == 0)
            {
                ContaCorrente novaConta = new ContaCorrente();
                novaConta.Titular = new Cliente(textoTitular.Text);
                novaConta.Numero = Convert.ToInt32(textoNumero.Text);
                this._formPrincipal.AdicionaConta(novaConta);

            }
            else 
            {
                ContaPoupança novaConta = new ContaPoupança();
                novaConta.Titular = new Cliente(textoTitular.Text);
                novaConta.Numero = Convert.ToInt32(textoNumero.Text);
                this._formPrincipal.AdicionaConta(novaConta);

            }
            this.Close();
        }

        public void comboTipoConta_SelectedIndexChanged(object sender, EventArgs e)
        {
                       
        }
    }

    public class TipoConta
    {
        string tipoConta { get; set; }
        string corrente { get; set; }
        string poupanca { get; set; }
    }
}
